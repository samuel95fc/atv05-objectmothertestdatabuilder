package escola;

import domain.Aluno;
import persistence.AlunoDAO;
import util.DateHelper;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

import business.AlunoBO;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AlunoBOIntegradoTest {
	
	private DateHelper dateHelperMock;
	private AlunoDAO alunoDAOMock;
	private AlunoBO alunoBO;

	@BeforeEach
	public void setup() {
		dateHelperMock = Mockito.mock(DateHelper.class);
		alunoDAOMock = Mockito.mock(AlunoDAO.class);
		alunoBO = new AlunoBO(alunoDAOMock, dateHelperMock);
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
	 * anos. Caso de teste 
	 * # | entrada 				 | saida esperada 
	 * 1 | aluno nascido em 2003 |	 * 16
	 */
	public void testarCalculoIdadeAluno1() {
		
		Integer matricula = 1;

		Mockito.doReturn(2019).when(dateHelperMock).obterAnoAtual();

		Aluno aluno = AlunoBuilder.umAluno().comAnoNascimento(2003).Builder();
		Mockito.doReturn(aluno).when(alunoDAOMock).encontrarPorMatricula(matricula);

		Integer esperado = 16;

		Integer atual = alunoBO.calcularIdade(matricula);

		assertEquals(esperado, atual);

		Mockito.verify(dateHelperMock).obterAnoAtual();
		Mockito.verify(alunoDAOMock).encontrarPorMatricula(matricula);

	}

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	public void testarAtualizacaoAlunosAtivos() {
		
		Aluno alunoAtivo = AlunoBuilder.umAluno().ativo().Builder();

		alunoBO.atualizar(alunoAtivo);

		Mockito.verify(alunoDAOMock).salvar(alunoAtivo);

	}

}
