package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AbstractDAO {
	private static final String USER = "postgres";
	private static final String PASSWORD = "postegresql";
	private static final String STRING_CONNECTION = "jdbc:postgresql://localhost:5432/atividade05";

	private Connection connection = null;

	protected Connection getConnection() {
		if (connection == null) {
			conectar();
		}
		return connection;
	}

	private void conectar() {
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(STRING_CONNECTION, USER, PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}
